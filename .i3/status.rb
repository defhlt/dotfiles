#!/usr/bin/ruby
# encoding: utf-8
# Created: 2013-04-24 13:22:17 +0300 by ice
require 'json'
require 'net/http'
require 'date'

Thread.new do
  while (t = STDIN.gets)
    t = t[/{.*}/]
    begin
      d = JSON.parse("#{t}");
      x, y = d.values_at "x", "y";
      name = d["name"]
      case name
      when "weather"
        `firefox openweathermap.com/city/709930`
      when "time"
        #`urxvt -name floating -e sh -c "ncal -3M && zsh" -geometry 80x20`
        `notify-send "\`ncal -Mh\`"`
      end
    rescue Exception => e
      msg = e.class.to_s
    else
      msg = t
    end
    #IO.write('/tmp/st', msg)
  end
end

def refresh_i3status
  `killall -SIGUSR1 i3status`
end

def weather_temp
  u = 'http://api.openweathermap.org/data/2.5/weather?q=dnipropetrovsk'
  begin
    response = JSON.parse(Net::HTTP.get(URI.parse(u)))
    temp, humidity = response["main"].values_at "temp", "humidity"
    suntime = response["sys"].values_at("sunrise", "sunset").select { |s| s > Time.now.to_i }.min
    if suntime
      suntime = Time.at(suntime).strftime "%R"
    end
    "%2.0f°%d%%;%s" % [temp - 273.15, humidity, suntime]
  rescue
    `notify-send weather upd err`
  end
end


outside_temp = "_"
Thread.new do
  loop do
    outside_temp = weather_temp
    sleep 300
  end
end


def lang_formater(s)
  color = case s
          when '0'
            ['EN', "#00FFFF"]
          when '1'
            ['UK', "#FFFF00"]
          else
            ['??', "#c1c1c1"]
          end
end

def brightness
  cur = IO.readlines('/sys/class/backlight/acpi_video0/actual_brightness')[0]
  max = IO.readlines('/sys/class/backlight/acpi_video0/max_brightness')[0]
  cur = cur.chomp.rjust(2, '0')
  max.chomp!
  "br:#{cur}/#{max}"
end

lang = case `xset -q | grep -A 0 'LED' | cut -c59-67`
       when /^0+$/
         lang_formater '0'
       else
         lang_formater '1'
       end


Thread.new do
  sleep 5
  interface = 'ru.gentoo.KbddService'
  member = 'layoutChanged'
  mon = open "| dbus-monitor --monitor \"sender='#{interface}',member='#{member}'\""
  loop do
    str = mon.gets
    if str =~ /layoutChanged/
      g = mon.gets[/.\Z/]
      lang = lang_formater(g)
    end
    refresh_i3status
  end
end

def swap_used
  "swp:%s" % IO.readlines("/proc/swaps")[1..-1].map do |line|
    line.split("\t")[2].to_i/1024
  end.inject(:+)
end

def volume
  inf = `pactl list sinks`
  inf[/^Sink\s#(\d+)$.{,200}Analog/m]
  sink_n = $1

  inf[/^Sink ##{sink_n}.*?Mute: (\w+).*Volume: 0:\s+(\d+)% 1:\s+(\d+)%$/m]
  mute = $1
  begin
    v1 = $2.rjust(3, '0')
    v2 = $3.rjust(3, '0')
  rescue
    return 'err'
  end

  if mute == 'yes'
    te = 'MUTE:'
    co = '#ff0101'
  else
    te = 'vol:'
    co = '#dcdcdc'
  end
  if v1 == v2
    n = (v1.to_f/10).ceil - 1
    n = 0 if n < 0
    v1 = (?| * v1.to_f.fdiv(10)).ljust(10, ?-)
    ["#{te}#{v1}", co]
  else
    ["#{te}#{v1}|#{v2}", co]
  end
end

def uptime
  secs = IO.read("/proc/uptime").split(" ").first.to_f
  h = (secs / 3600).floor
  #m = ((in_hours - h) * 60).floor
  "#{h}h up"
end


i3 = open("| i3status")
i3.gets # skip 1st line
puts '{"version":1, "click_events":true}'
2.times { puts i3.gets; refresh_i3status }
loop do
  i3.gets.chomp[/^,?(.*)$/]
  j =  JSON.parse($1)

  j.insert(0, {"name" => 'swap', "full_text" => swap_used})
  j.insert(0, {"name" => 'brightness', "full_text" => brightness})

  la, co = lang
  j.insert(0, {"name" => "lang", "full_text" => la, "color" => co})

  te, co = volume()
  j.insert(0, {"name" => "vol", "full_text" => te, "color" => co})

  j.insert(0, {"name" => "uptime", "full_text" => uptime})
  j.insert(0, {"name" => "weather", "full_text" => outside_temp})

  j.map! do |el|
    el['color'] = '#01ffff' if el['name'] == 'time'
    el
  end

  IO.write('/tmp/i3log', j)

  puts  ",#{j.to_json}"
end
