export TERM=screen-256color
export J2OBJC_ROOT=/home/ice/dev/j2objc-all/j2objc-1.1
autoload -U compinit promptinit colors zcalc
compinit
promptinit
colors

export TERMINAL=gnome-terminal
export ANDROID_HOME=~/Android/Sdk
#prompt oliver

# most pager
# if [[ -x `which most` ]]; then
#   export PAGER=most
# else
#   echo "Pls install \`most'"
# fi

##########
#  Keys  #
##########
# bindkey "^[[1~" beginning-of-line
# bindkey "^[[4~" end-of-line
# ^left and ^right; works in terminator
bindkey ';5D' emacs-backward-word
bindkey ';5C' emacs-forward-word

###########
# Aliases #
###########



# 10 second wait if you do something that will delete everything.  I wish I'd had this before...
setopt RM_STAR_WAIT


# only fools wouldn't do this ;-)
export EDITOR="nvim"
# But still use emacs-style zsh bindings
bindkey -e

# # beeps are annoying
# # setopt NO_BEEP

# # Completion
zstyle ':completion:*' menu select
# # Lowcase matches updcase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''
# zstyle ':completion:*' completer _oldlist _expand _force_rehash _complete
# zstyle ':completion:*' completer _expand _force_rehash _complete _approximate _ignored

# # generate descriptions with magic.
zstyle ':completion:*' auto-description 'specify: %d'

# # Don't prompt for a huge list, page it!
# # zstyle ':completion:*:default' list-prompt '%S%M matches%s'

# # Don't prompt for a huge list, menu it!
zstyle ':completion:*:default' menu 'select=0'

# # Have the newer files last so I see them first
# zstyle ':completion:*' file-sort modification reverse

# # color code completion!!!!  Wohoo!
zstyle ':completion:*' list-colors "=(#b) #([0-9]#)*=36=31"

# unsetopt LIST_AMBIGUOUS
# setopt  COMPLETE_IN_WORD

# # Separate man page sections.  Neat.
# zstyle ':completion:*:manuals' separate-sections true

# # Egomaniac!
zstyle ':completion:*' list-separator '->'

# # complete with a menu for xwindow ids
# zstyle ':completion:*:windows' menu on=0
# zstyle ':completion:*:expand:*' tag-order all-expansions

# # more errors allowed for large words and fewer for small words
# zstyle ':completion:*:approximate:*' max-errors 'reply=(  $((  ($#PREFIX+$#SUFFIX)/3  ))  )'

# # Errors format
# zstyle ':completion:*:corrections' format '%B%d (errors %e)%b'

# # Don't complete stuff already on the line
# zstyle ':completion::*:(rm|vi):*' ignore-line true

# # Don't complete directory we are already in (../here)
# zstyle ':completion:*' ignore-parents parent pwd

# zstyle ':completion::approximate*:*' prefix-needed false

# # oh wow!  This is killer...  try it!
# bindkey -M vicmd "q" push-line

# # #{{{ History Stuff

# # Where it gets saved
HISTFILE=~/.zsh_history

# # # Remember about a years worth of history (AWESOME)
SAVEHIST=10000
HISTSIZE=10000

# # # Don't overwrite, append!
setopt APPEND_HISTORY

# # # Write after each command
setopt INC_APPEND_HISTORY

# # # Sucks: share history between multiple shells
# setopt SHARE_HISTORY

# # # If I type cd and then cd again, only save the last one
setopt HIST_IGNORE_DUPS

# # # Even if there are commands inbetween commands that are the same, still only save the last one
setopt HIST_IGNORE_ALL_DUPS

# # # Pretty    Obvious.  Right?
setopt HIST_REDUCE_BLANKS

# # # If a line starts with a space, don't save it.
setopt HIST_IGNORE_SPACE
# setopt HIST_NO_STORE

# # # When using a hist thing, make a newline show the change before executing it.
# setopt HIST_VERIFY

# # # Save the time and how long a command ran
setopt EXTENDED_HISTORY

# # setopt HIST_SAVE_NO_DUPS
# # setopt HIST_EXPIRE_DUPS_FIRST
# # setopt HIST_FIND_NO_DUPS

# # #}}}




function fract {
   local lines columns colour a b p q i pnew
   ((columns=COLUMNS-1, lines=LINES-1, colour=0))
   for ((b=-1.5; b<=1.5; b+=3.0/lines)) do
       for ((a=-2.0; a<=1; a+=3.0/columns)) do
           for ((p=0.0, q=0.0, i=0; p*p+q*q < 4 && i < 32; i++)) do
               ((pnew=p*p-q*q+a, q=2*p*q+b, p=pnew))
           done
           ((colour=(i/4)%8))
            echo -n "\\e[4${colour}m "
        done
        echo
    done
}


# if [[ "$MC_SID" != "" || "$MC_CONTROL_PID" != "" ]]; then
#         bindkey "^J" accept-line
# else
#         bindkey "^J" self-insert
# fi

expand-or-complete-with-dots() {
  echo -n "\e[31m......\e[0m"
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
bindkey "^I" expand-or-complete-with-dots

# # Prompt

# Line numbers and highlight in less
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
export LESS=' -RFX '

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
PATH=$HOME/bin:$PATH
PATH=$HOME/dev/android-sdk-linux/platform-tools:$PATH
PATH=$HOME/dev/android-sdk-linux/tools:$PATH
PATH=$HOME/dev/android-ndk-r9:$PATH

### PROMPT {
# git info
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr 'M'
zstyle ':vcs_info:*' unstagedstr 'M'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{6}[%F{2}%b%F{3}|%F{1}%a%F{6}]%f '
zstyle ':vcs_info:*' formats '%F{6}[%F{2}%b%F{6}] %F{2}%c%F{3}%u%f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
zstyle ':vcs_info:*' enable git
+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
    git status --porcelain | grep '??' &> /dev/null ; then
    hook_com[unstaged]+='%F{3}u%f'
  fi
}

precmd () { vcs_info; }
#precmd () { vcs_info; schedprompt }
#PROMPT='%B%F{6}[%F{7}%n%F{6}] %F{7}%3~ ${vcs_info_msg_0_} %f%#%b '
#PROMPT='%B%F{0}%T %B%F{6}{%F{7}%n%F{6}} %F{7}%3~ %f%#%b%(?.. %B%F{1}%?)%f%b '
#PROMPT='%B%F{2}> %T %B%F{6}{%F{7}%n%F{6}} %F{7}%3~${vcs_info_msg_0_} %f%#%b '
#PROMPT='\033]0;\007%B%F{2}> %B%F{6}{%F{7}%n%F{6}} %F{7}%3~${vcs_info_msg_0_} %F{2}:%T %f%#%b '
PROMPT='%B%F{2}-> %U%F{2}%n%B %F{7}%3~${vcs_info_msg_0_}%u %f%#%b '

# # current time in prompt schedprompt() {
#   emulate -L zsh
#   zmodload -i zsh/sched
# 
#   # Remove existing event, so that multiple calls to
#   # "schedprompt" work OK.  (You could put one in precmd to push
#   # the timer 30 seconds into the future, for example.)
#   integer i=${"${(@)zsh_scheduled_events#*:*:}"[(I)schedprompt]}
#   (( i )) && sched -$i
# 
#   # Test that zle is running before calling the widget (recommended
#   # to avoid error messages).
#   # Otherwise it updates on entry to zle, so there's no loss.
#   zle && zle reset-prompt
# 
#   #vcs_info
# 
#   # This ensures we're not too far off the start of the minute
#   sched +30 schedprompt
# }
# 
# schedprompt

# }
#

# Notification when log-running command exits
#
function active-window-id {
  echo `xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}'`
}

# end and compare timer, notify-send if needed
function notifyosd-precmd() {
  if [ ! -z "$cmd" ]; then
    cmd_end=`date +%s`
    ((cmd_time=$cmd_end - $cmd_start))
  fi
  if [ ! -z "$cmd" -a $cmd_time -gt 10 ]; then
    #if [ "$window_id_before" != "$(active-window-id)" ]; then
      #notify-send -i utilities-terminal -u normal "$cmd_basename completed" "\"$cmd\" took $cmd_time seconds"
    #fi
    echo "\a"
    unset cmd
  fi
}
# make sure this plays nicely with any existing precmd
precmd_functions+=( notifyosd-precmd )
# get command name and start the timer
function notifyosd-preexec() {
  cmd=$1
  cmd_basename=${cmd[(ws: :)1]}
  cmd_start=`date +%s`
}
# make sure this plays nicely with any existing preexec
preexec_functions+=( notifyosd-preexec )

function notify-send-tmux {
  if [ -f /tmp/tmux-win-id ]; then
    prev_win_id=`cat /tmp/tmux-win-id`
    if [ "$prev_win_id" != "$(active-window-id)" ]; then
      notify-send -u critical "weechat highlight"
    fi
  fi
}

# ^_^
export ANDROID_SDK=~/dev/android-sdk-linux


# Ensures that $terminfo values are valid and updates editor information when
# the keymap changes.
function zle-keymap-select zle-line-init zle-line-finish {
  # The terminal must be in application mode when ZLE is active for $terminfo
  # values to be valid.
  if (( ${+terminfo[smkx]} )); then
printf '%s' ${terminfo[smkx]}
  fi
if (( ${+terminfo[rmkx]} )); then
printf '%s' ${terminfo[rmkx]}
  fi

zle reset-prompt
  zle -R
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select


source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


# /tmp -> 'cd /tmp'
setopt autocd

#CDPATH=$CDPATH:~/dev:~/dev/moai-dev:~/dev/moai-dev/ant
#CDPATH=$CDPATH:~/Downloads:~/AndroidstudioProjects
#CDPATH=$CDPATH:~/Downloads:~/dev/android
#CDPATH=$CDPATH:~/Downloads:~/dev/yalantis-projects
CDPATH=$CDPATH:~/proj
CDPATH=$CDPATH:~/proj/etc
CDPATH=$CDPATH:~/proj/yalantis-projects
CDPATH=$CDPATH:~/proj/yalantis-ruby

export NDK_ROOT=~/dev/android-ndk-r9

source ~/.aliases.sh

command_not_found_handler() {
  if [[ $@ =~ '^[0-9\*\-\+\/ ]+$' ]]; then
    exp="puts("$@")"
    ruby -e "$exp"
    return 0
  else
    return 127
  fi
}




## if mode indicator wasn't setup by theme, define default
#if [[ "$MODE_INDICATOR" == "" ]]; then
#MODE_INDICATOR="%{$fg_bold[red]%}<%{$fg[red]%}<<%{$reset_color%}"
#fi
#function vi_mode_prompt_info() {
#echo "${${KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
#}
## define right prompt, if it wasn't defined by a theme
##if [[ "$RPS1" == "" && "$RPROMPT" == "" ]]; then
#RPS1='$(vi_mode_prompt_info)'
##fi



export PATH=/home/ice/.rvm/gems/ruby-2.2.2/bin:/home/ice/.rvm/gems/ruby-2.2.2@global/bin:/home/ice/.rvm/rubies/ruby-2.2.2/bin:/home/ice/dev/android-ndk-r9:/home/ice/dev/android-sdk-linux/tools:/home/ice/dev/android-sdk-linux/platform-tools:/home/ice/bin:/home/ice/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/ice/.rvm/bin:/home/ice/.vimpkg/bin

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/ice/.sdkman"
[[ -s "/home/ice/.sdkman/bin/sdkman-init.sh" ]] && source "/home/ice/.sdkman/bin/sdkman-init.sh"
export PATH="/home/ice/.rvm/gems/ruby-2.3.1/bin:/home/ice/.rvm/gems/ruby-2.3.1@global/bin:/home/ice/.rvm/rubies/ruby-2.3.1/bin:$PATH"
export GEM_HOME='/home/ice/.rvm/gems/ruby-2.3.1'
export GEM_PATH='/home/ice/.rvm/gems/ruby-2.3.1:/home/ice/.rvm/gems/ruby-2.3.1@global'
export MY_RUBY_HOME='/home/ice/.rvm/rubies/ruby-2.3.1'
export IRBRC='/home/ice/.rvm/rubies/ruby-2.3.1/.irbrc'
unset MAGLEV_HOME
unset RBXOPT
export RUBY_VERSION='ruby-2.3.1'
