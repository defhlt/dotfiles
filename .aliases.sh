alias l="ls --color -Fh"
alias ll="ls --color -Fhl"
alias lla="ls --color -Fhla"
alias dh="dh -h"
alias du="du -h"
alias grep="grep --color"
alias v="nvim"
alias nv="nvim"
alias vs="vim -S"
alias cx="chmod +x"
alias cj="clojure"
alias octave="octave -q"
alias mogrify-twitter="mogrify -resize '1250000@>' -quality 85 *"
alias tmux="active-window-id > /tmp/tmux-win-id; tmux"
alias rs="redshift -o -l 48.45:34.98 -v "
alias clj=clojure
alias mn='~/temp/moai-dev/release/linux/x64/bin/moai main.lua'

alias gt="ack-grep --passthru"
alias q=exit
alias ra=ranger
alias githome="git --git-dir=.gith"
alias cd-="cd -"
alias cal="ncal -3M"
#alias sshproxy='ssh -p222 -C2TnN -D 8888 mlatu@3226069999'
alias sshproxy='ssh -C2TnN -D 8888 mlatu@t.sql01.com'
alias hr="ruby -e 'puts ?**`tput cols`'"
alias mp3_enc_fix="find -iname '*.mp3' -print0 | xargs -0 mid3iconv -e CP1251 --remove-v1"
alias and-studio=~/dev/android-studio/bin/studio.sh
alias wl="vim -O ~/worklog/{todo,progress,done}.markdown"
alias gp="git status -uno | fpp"
alias gb="git log --author=$1 --pretty=tformat: --numstat | gawk '{ add += $1; subs += $2; loc += $1 - $2 } END { printf \"added lines: %s removed lines: %s total lines: %s\n\", add, subs, loc }' -"
alias g="git"
