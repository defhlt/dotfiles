set nocompatible " Be iMproved
filetype on
filetype plugin on
filetype indent on
syntax on
set encoding=utf-8 " use utf-8 everywhere
set fileencoding=utf-8 " use utf-8 everywhere
set termencoding=utf-8 " use utf-8 everywhere
set bg=dark
"set t_Co=256
colorscheme delek




let mapleader=","

" Bubble single lines
nmap <C-Down> :m+<CR>
nmap <C-Up> :m-2<CR>
" Bubble multiple lines
vmap <C-Down> :m'>+<CR> gv
vmap <C-Up> :m'<-2<CR> gv

" Toggle spell checking
set nospell
nmap <leader>s :set spell!<CR>

set nolist
nmap <leader>l :set list!<CR>

nmap <leader><space> :nohlsearch<CR>
nmap <leader>w :set wrap!<CR>
nmap <leader>d "_d

"Annoying key
imap <Insert> <Nop>
"map Q

"Prevent cursor jumping
map <silent> <PageUp> 1000<C-U>
map <silent> <PageDown> 1000<C-D>
imap <silent> <PageUp> <C-O>1000<C-U>
imap <silent> <PageDown> <C-O>1000<C-D>

" Indentation
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

if has("autocmd")
  " needed for indent = command
  filetype indent on

  " Enable filetype detection
  filetype plugin indent on

  " Restore cursor position
  autocmd BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
endif

" if &t_Co > 2 || has("gui_running")
" 	" Enable syntax highlighting
" 	syntax on
" endif


"Extra
set mouse=a " Enable the mouse for all modes
set mousehide " ?
set mousemodel=popup " Don't hide terminal menu

" Searching, Substituting
set incsearch " Show search matches as you type
set ignorecase " Ignore case when searching
set smartcase " don't ugnore case for CAPITAL LETTERS
set hlsearch " Highlight search results
highlight Search ctermfg=black  ctermbg=darkyellow
set noshowmatch " Don't jump to bracket - it can be annoying

set title " Set descriptive window/terminal title
set titlestring=%f%(\ [%M]%)
"                |      |
"                |      +-- Modification status
"                +--------- Filename

" Statusline
set showcmd " Display the command in the last line
set showmode " Display the current mode in the last line
set laststatus=2 " Always show status line

set nostartofline

" Use the same symbols as TextMate for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬
set listchars+=precedes:<,extends:>
"<TAB>
highlight SpecialKey ctermfg=darkgray
"<CR>
highlight NonText ctermfg=darkgray

" Statusline
if has('statusline')
  function! SetStatusLineStyle()
    if &stl == '' || &stl =~ 'synID'
      let &stl="%f %([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]}%{'~'[&pm=='']}%=#%n %l/%L,%c%V "
    else
      let &stl="%F %y%([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]} (%{synIDattr(synID(line('.'),col('.'),0),'name')})%=#%n %l/%L,%c%V "
    endif
  endfunc
  " switch between the normal and vim-debug modes in
  " the status line
  map _ds :call SetStatusLineStyle()<CR>
  call SetStatusLineStyle()
endif
call SetStatusLineStyle()

" Folding
set fdm=syntax
set foldnestmax=5
set foldcolumn=0
hi folded ctermbg=black cterm=none ctermfg=gray
hi foldcolumn ctermbg=black ctermfg=darkgray

function! VisualSelectionSize()
  " Taken somewhere from the web
  if mode() == "v"
    " Exit and re-enter visual mode, because the marks
    " ('< and '>) have not been updated yet.
    exe "normal \<ESC>gv"
    if line("'<") != line("'>")
      return (line("'>") - line("'<") + 1) . ' lines'
    else
      return (col("'>") - col("'<") + 1) . ' chars'
    endif
  elseif mode() == "V"
    exe "normal \<ESC>gv"
    return (line("'>") - line("'<")  + 1) . ' lines'
  elseif mode() == "\<C-V>"
    exe "normal \<ESC>gv"
    return (line("'>") - line("'<") + 1) . 'x' . (abs(col("'>") - col("'<")) + 1) . ' block'
  else
    return ''
  endif
endfunction

function! IsVSelectionSizeGt1()
  n = VisualSelectionSize()
  if n > 1
    return 1
  else
    return 0
  endif
endfunction

function SmartSelect()
  exe "normal vi\""
  if VisualSelectionSize() > 1
    return 1
  else
    exe "normal i'"
    if VisualSelectionSize() > 1
      return 1
    else
      exe "normal i("
      if VisualSelectionSize() > 1
        return 1
      else
        " Exit visual mode
        exe "normal v"
      endif
    endif
  endif
endfunction

function SmartSelectX()
  exe "normal mxvi\""
  let a = VisualSelectionSize()
  exe "normal v`xvi'"
  let b = VisualSelectionSize()
  exe "normal v`xvi)"
  let c = VisualSelectionSize()
  if a < b && b < c && a >= 2
    exe "normal v`xvi\""
  elseif b < c && b >= 2
    exe "normal v`xvi'"
  elseif c >= 2
    exe "normal v`xvi`"
  endif
endfunction


" Templates {
"
" Ruby
" autocmd bufnewfile *.rb :.!which ruby
" autocmd bufnewfile *.rb :1s/\v(.*)/#!\1\r/
" autocmd bufnewfile *.rb :.!date
" autocmd bufnewfile *.rb :2s/\v(.*)/# Created: \1\r/
" autocmd bufnewfile *.rb exe "normal a# encoding: utf-8\<CR>"
" autocmd bufnewfile *.rb exe "normal oSTDOUT.sync = true\<CR>"
" autocmd bufnewfile *.rb exe "normal oif __FILE__ == $0\<CR>\<CR>end"
" autocmd bufnewfile *.rb exe "normal k"
autocmd bufnewfile *.rb exe NewFile("ruby")


function! NewFile(type)
ruby <<EOF
if VIM.evaluate('a:type') == 'ruby'
  head = %Q{#!#{(`which ruby -w`).chomp}
  # encoding: utf-8
  # Created: #{Time.now.to_s} by #{`whoami`}
  STDOUT.sync = true
  %w{
  # requirements
  }.each { |r| require r}

  exit if __FILE__ != $0


  }
end
head.split("\n").each_with_index do |l, i|
VIM::Buffer.current.append(i, l)
end
EOF
endfunction

" Clojure
autocmd bufnewfile *.clj :.!which clojure
autocmd bufnewfile *.clj :1s/\v(.*)/#!\1\r/
autocmd bufnewfile *.clj :.!date
autocmd bufnewfile *.clj :2s/\v(.*)/; Created: \1\r/
autocmd bufnewfile *.clj exe "normal a; encoding: utf-8\<CR>"
" }

" Vundle
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'ruby'
Bundle 'vim-ruby-debugger'
Bundle 'vim-scripts/vim-auto-save'

" required!
Bundle 'gmarik/vundle'

Bundle 'scrooloose/nerdcommenter'
Bundle 'vim-ruby/vim-ruby'
Bundle 'VimClojure'

" For auto 'ends' etc:
Bundle 'endwise.vim'
Bundle 'Gist.vim'

Bundle 'WebAPI.vim'

Bundle 'ecomba/vim-ruby-refactoring'
"Bundle 'ruby-matchit'
Bundle 'matchit.zip'

Bundle 'VimOrganizer'

Bundle 'VimOutliner'

Bundle 'textobj-user'
Bundle 'textobj-rubyblock'
Bundle 'surround.vim'
"Bundle 'ervandew/supertab'

Bundle 'https://github.com/vim-scripts/mru.vim.git'

Bundle 'https://github.com/tpope/vim-fireplace'
Bundle 'https://github.com/tpope/vim-fireplace-easy'
Bundle 'https://github.com/tpope/vim-classpath'
Bundle 'https://github.com/terryma/vim-multiple-cursors'

Bundle 'svermeulen/vim-extended-ft'

Bundle 'kien/ctrlp.vim'

Bundle 'mhinz/vim-startify'

Bundle 'Shougo/neocomplete.vim'

Bundle 'vim-scripts/AutoComplPop'
Bundle 'https://github.com/sjl/gundo.vim.git'
Bundle 'https://github.com/Raimondi/delimitMate.git'

Bundle 'https://github.com/Lokaltog/vim-easymotion.git'

"Lua
Bundle 'https://github.com/xolox/vim-lua-ftplugin.git'
Bundle 'https://github.com/xolox/vim-misc'
"Bundle 'bpowell/vim-android'
"Bundle 'artemave/slowdown.vim'
" original repos on github
"Bundle 'tpope/vim-fugitive'
"Bundle 'Lokaltog/vim-easymotion'
"Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
"Bundle 'tpope/vim-rails.git'
" vim-scripts repos
"Bundle 'L9'
"Bundle 'FuzzyFinder'
" non github repos
"Bundle 'git://git.wincent.com/command-t.git'
"
" Shaders syntax hl:
Bundle "glsl.vim"

Bundle "vimux"

Bundle 'vim-rails'
Bundle 'vim-bundler'

" parameter text object
"Bundle 'https://github.com/b4winckler/vim-angry.git'
"                " ...
"
filetype plugin indent on " required!
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""   
" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line" long line"

"
"hi BadWhiteSpace ctermbg=red
"match BadWhitespace /\s\+$/

autocmd BufReadPost * match BadWhitespace /\s\+$/
autocmd InsertEnter * match BadWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match BadWhitespace /\s\+$/
highlight BadWhitespace ctermbg=white

"hi BetterColorColumn ctermbg=white
"2match BetterColorColumn /\%80c/


inoremap jj <ESC>
cnoremap jj <ESC>

set showmode
set showcmd
set wildmenu
set ttyfast " Watchout if using ssh
" Save and restore undo hist:
set undofile

" Line numbers
set numberwidth=3
set relativenumber
set number
highlight LineNr ctermfg=darkgrey guifg=gray

set cursorline
highlight CursorLineNr ctermfg=yellow cterm=bold
highlight CursorLine  gui=none cterm=none term=none

" Fix vim's terrible regexp scheme
nnoremap / /\v
vnoremap / /\v
vnoremap ? ?\v
nnoremap ? ?\v
set gdefault "s///g by default

"
" Text wrap
"
" We do follow style guides, right?
set nowrap
set textwidth=0 " !!!
set formatoptions=qrn1tcj
"set colorcolumn=80
"hi ColorColumn ctermbg=blue guibg=darkgray





" Reselect the text that just was pasted:
nnoremap <leader>v `[V`]

highlight SpellBad cterm=bold ctermfg=white ctermbg=red

set scrolloff=3

" Gvim
if has("gui_running")
  "set guifont=Terminus:h12:cRUSSIAN " Cyrillic
  set guifont=Terminus\ 12
  set guioptions=egmlLt
  hi Normal guifg=gray guibg=black
  hi NonText guibg=black
  hi Cursor guibg=green
endif

set foldlevel=9

" From docs:
" If you like "Y" to work from the
" cursor to the end of line (which is more logical,
" but not Vi-compatible) use ":map Y y$".
map Y y$

set tabpagemax=30 " 30 instead of 10 concurrent tabs

hi MatchParen cterm=underline ctermfg=none ctermbg=none


function! RubyMethodFold(line)
  let line_is_method_or_end = synIDattr(synID(a:line,1,0), 'name') == 'rubyMethodBlock'
  let line_is_def = getline(a:line) =~ '\s*def '
  return line_is_method_or_end || line_is_def
endfunction

set fdm=indent
set foldexpr=RubyMethodFold(v:lnum)

function EvalBuf()
  let @" = expand("%:p") "Copy fullpath
  execute "7new"
  call feedkeys(":.!".@")
  call feedkeys("\<CR>")
endfunction

nmap <leader>e :call EvalBuf()<CR>



"ruby
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1
"improve autocomplete menu color
"highlight Pmenu ctermbg=238 gui=bold
"

" w!! - write with root permitions
cnoreabbrev w!! w !sudo tee % >/dev/null


" Clojure
let g:vimclojure#HighlightBuiltins = 1
let g:vimclojure#ParenRainbow = 1
let g:vimclojure#DynamicHighlighting = 1

" vimorg
au! BufRead,BufWrite,BufWritePost,BufNewFile *.org
au BufEnter *.org call org#SetOrgFileType()
au BufRead *.glsl :set filetype=glsl

command Wroot :w !sudo tee %<CR>
command W :write<CR>


" Folding for Ruby
"
" ,z -- Show only last search
" ,zz -- Show only "describe ..." and "it ..." lines in specs
" ,zd -- Show only "class ..." and "def ..." lines in Ruby files
" zR -- Remove all folds
"
" From http://vim.wikia.com/wiki/Folding_with_Regular_Expression
nnoremap ,z :setlocal foldexpr=(getline(v:lnum)=~@/)?0:(getline(v:lnum-1)=~@/)\\|\\|(getline(v:lnum+1)=~@/)?1:2 foldmethod=expr foldlevel=0 foldcolumn=2<CR>

" Then variations on that, with different searches ...
"
" Fold spec files, displaying "describe ..." and "it ..." lines
function! FoldSpec()
  let @/='\(describe.*do$\|it.*do$\)'
  setlocal foldexpr=(getline(v:lnum)=~@/)?0:(getline(v:lnum-1)=~@/)\\|\\|(getline(v:lnum+1)=~@/)?1:2 foldmethod=expr foldlevel=0 foldcolumn=2
endfunction
map ,zz :call FoldSpec()<CR>

" Fold Ruby, showing class and method definitions
function! FoldDefs()
  let @/='\(module\ \|class\ \|has_many\ \|belongs_to\ \|_filter\ \|helper\ \|belongs_to\ \|def\ \|private\|protected\)'
  setlocal foldexpr=(getline(v:lnum)=~@/)?0:(getline(v:lnum-1)=~@/)\\|\\|(getline(v:lnum+1)=~@/)?1:2 foldmethod=expr foldlevel=0 foldcolumn=2
endfunction
map ,zd :call FoldDefs()<CR>

" Do not beep
set vb

" Encoding menu {
set wcm=<Tab>
menu Encoding.koi8-r :e+enc=koi8-r ++ff=unix<CR>
menu Encoding.koi8-u :e+enc=koi8-u ++ff=unix<CR>
menu Encoding.windows-1251 :e+enc=cp1251 ++ff=dos<CR>
menu Encoding.cp866 :e+enc=cp866 ++ff=dos<CR>
menu Encoding.utf-8 :e+enc=utf8 <CR>
map <F8> :emenu Encoding.<TAB>
" }


" Cyrillic support (use C+^ in insert mode)
"set keymap=russian-jcukenwin
set keymap=russian-jcukenwin
"ukrainian-jcuken
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

"hi cursorline cterm=underline ctermbg=243
"au InsertEnter * set nocursorline
"au InsertLeave * set cursorline
"
nnoremap ; :
vnoremap ; :

"autocmd FileType ruby map <leader>r :w<CR>:!clear && ruby -w %<CR>
"autocmd FileType ruby source /home/ice/.vim/bundle/vim-ruby/compiler/ruby.vim<CR>

nmap <silent>]c :cnext<CR>
nmap <silent>[c :cprevious<CR>
nmap <silent>[C :cfirst<CR>
nmap <silent>]C :clast<CR>
nmap <silent>]b :bnext<CR>
nmap <silent>[b :bprevious<CR>

nmap <leader>1 :cprev<CR>
nmap <leader>2 :cnext<CR>

"autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window %")


"Added by android-vim:
set tags+=/home/ice/.vim/tags
autocmd Filetype java setlocal omnifunc=javacomplete#Complete
let g:SuperTabDefaultCompletionType = 'context'

set novisualbell

" Clojure
"let vimclojure#WantNailgun = 1
"let vimclojure#NailgunClient = "ng-nailgun"
"

set splitright
set splitbelow

" extended ft [
let g:ExtendedFTUseDefaults=0

nmap <silent> <Space> <plug>ExtendedFtRepeatSearchForward
nmap <silent> <Backspace> <plug>ExtendedFtRepeatSearchBackward
nmap <silent> f <plug>ExtendedFtSearchFForward
nmap <silent> F <plug>ExtendedFtSearchFBackward
nmap <silent> t <plug>ExtendedFtSearchTForward
nmap <silent> T <plug>ExtendedFtSearchTBackward

xmap <silent> <Space> <plug>ExtendedFtVisualModeRepeatSearchForward
xmap <silent> <Backspace> <plug>ExtendedFtVisualModeRepeatSearchBackward
xmap <silent> f <plug>ExtendedFtVisualModeSearchFForward
xmap <silent> F <plug>ExtendedFtVisualModeSearchFBackward
xmap <silent> t <plug>ExtendedFtVisualModeSearchTForward
xmap <silent> T <plug>ExtendedFtVisualModeSearchTBackward

omap <silent> <Space> <plug>ExtendedFtOperationModeRepeatSearchForward
omap <silent> <Backspace> <plug>ExtendedFtOperationModeRepeatSearchBackward
omap <silent> f <plug>ExtendedFtOperationModeSearchFForward
omap <silent> F <plug>ExtendedFtOperationModeSearchFBackward
omap <silent> t <plug>ExtendedFtOperationModeSearchTForward
omap <silent> T <plug>ExtendedFtOperationModeSearchTBackward
" ]

fun! RangerChooser()
    exec "silent !ranger --choosefile=/tmp/chosenfile " . expand("%:p:h") . " --selectfile=" . expand("%:p")
    if filereadable('/tmp/chosenfile')
        exec 'edit ' . system('cat /tmp/chosenfile')
        call system('rm /tmp/chosenfile')
    endif
    redraw!
endfun
map <leader>r :call RangerChooser()<CR>

" MOAI [
"autocmd FileType lua,glsl set makeprg=moai\ ./main.lua
autocmd FileType lua,glsl set makeprg=moai1.4\ ./main.lua
autocmd FileType lua,glsl map <leader>r :w<CR>:make<CR>
"autocmd FileType lua,glsl imap <leader>> <ESC>:w<CR>:make<CR>

" shaders
autocmd BufNewFile,BufRead,BufWrite *.fsh,*.vsh set ft=glsl
"autocmd QuickFixCmdPost [^l]* nested copen
"autocmd QuickFixCmdPost    l* nested lwindow
" ]


set sessionoptions-=options
Bundle 'wakatime/vim-wakatime'

":nnoremap <leader>d "=strftime("%c")<CR>P
":inoremap <leader>d <C-R>=strftime("%c")<CR>



" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

let @d='yyGop:s/ /\r14kddjddjjojjjojjjojkjjj$x15kjjjyyjp3jyyjp3jyypOjjddkkkkkkkkkkkkkkkkAmr,jA,j.j.jj.j.j.j.jj.j.j.j.jj.j.j.j18k:.,$s/,/f,GAfjjV18k"+y'

let g:auto_save = 0

set iskeyword+=-
set iskeyword+=$
execute pathogen#infect()
call pathogen#helptags()
